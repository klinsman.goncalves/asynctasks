package br.com.samples.estadosdobrasil;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.net.URL;

import br.com.samples.estadosdobrasil.util.NetworkUtil;

public class MainActivity extends AppCompatActivity {

    final String TAG = "MainActivity";

    ProgressBar progressBarLoading;
    TextView tvTextoExibido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvTextoExibido = findViewById(R.id.tv_texto_exibido);
        progressBarLoading = findViewById(R.id.pb_loading);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        getMenuInflater().inflate(R.menu.main, m);
        return super.onCreateOptionsMenu(m);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_web_service:
                callWebService();
                break;
            case R.id.menu_clear:
                clearText();
                break;
            case R.id.menu_evo:
                callWebServiceEvo();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void callWebServiceEvo() {
        URL url = NetworkUtil.buildUrlCredenciadasEvo();
        MinhaAsyncTask task = new MinhaAsyncTask();
        task.execute(url);
    }

    public void callWebService() {
        Log.d(TAG, "method callWebService");
        URL url = NetworkUtil.buildUrl("stf");
        MinhaAsyncTask task = new MinhaAsyncTask();
        task.execute(url);
    }

    public void mostrarLoading(){
        tvTextoExibido.setVisibility(View.GONE);
        progressBarLoading.setVisibility(View.VISIBLE);
    }

    public void esconderLoading(){
        tvTextoExibido.setVisibility(View.VISIBLE);
        progressBarLoading.setVisibility(View.GONE);
    }
    public void clearText() {
        Log.d(TAG, "method clearText");
        tvTextoExibido.setText("");
    }


    class MinhaAsyncTask extends AsyncTask<URL, Void, String> {

        @Override
        protected String doInBackground(URL... urls) {
            URL url = urls[0];
            Log.d(TAG, "url utilizada: " + url.toString());
            String json = null;
            try {
                json = NetworkUtil.getResponseFromHttpUrl(url);
                Log.d(TAG, "async task retorno: " + json);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return json;
        }

        @Override
        protected void onPreExecute() {
            mostrarLoading();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            esconderLoading();
            if(s == null){
                tvTextoExibido.setText("Ocorreu um erro");
            } else {
                tvTextoExibido.setText(s);
            }
        }
    }


}
